export class CreateUsuarioDTO {
    readonly name: string;
    readonly user: string;
    readonly pass: string;
    readonly createdAt: Date;
}