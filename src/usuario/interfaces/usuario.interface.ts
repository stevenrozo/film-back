import { Document } from "mongoose";

export interface Usuario extends Document {
    readonly name: string;
    readonly user: string;
    readonly pass: string;
    readonly createdAt: Date;
}