import { Schema } from "mongoose";

export const UsuarioSchema = new Schema(
{
    name: String,
    user: String,
    pass: String,
    createdAt: { type: Date, default: Date.now }
}
);

