import { Controller, Post, Res, HttpStatus, Body, Get, Param, NotFoundException, Delete, Query, Put} from '@nestjs/common';
import { UsuarioService } from "./usuario.service";

import { CreateUsuarioDTO } from "./dto/usuario.dto";

@Controller('usuario')
export class UsuarioController {

    constructor(private usuarioService: UsuarioService) { }

        // Add Product: /usuario/create
        @Post('/create')
        async createUser(@Res() res, @Body() createUsuarioDTO: CreateUsuarioDTO) {
            const product = await this.usuarioService.createUser(createUsuarioDTO);
            return res.status(HttpStatus.OK).json({
                message: 'User Successfully Created',
                product
            });
        }

            // @Get('/list')
        @Get('/')
        async getProducts(@Res() res) {
            const products = await this.usuarioService.getUsers();
            return res.status(HttpStatus.OK).json(products);
        }

        // GET single product: /product/5c9d46100e2e5c44c444b2d1
        @Get('/:user/:pass')
        async getProduct(@Res() res, @Param('user') user, @Param ('pass') pass) {
            const product = await this.usuarioService.getUser(user, pass);
            if (!product) throw new NotFoundException('User does not exist!');
            return res.status(HttpStatus.OK).json(product);
        }


}
