import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Usuario } from "./interfaces/usuario.interface";
import { CreateUsuarioDTO } from './dto/usuario.dto';

@Injectable()
export class UsuarioService {
    constructor(@InjectModel('Usuario') private readonly usuarioModel: Model<Usuario>) {}

        // Post a single user
        async createUser(createUsuarioDTO: CreateUsuarioDTO): Promise<Usuario> {
            const newProduct = new this.usuarioModel(createUsuarioDTO);
            return newProduct.save();
        }

            // Get all users
        async getUsers(): Promise<Usuario[]> {
            const products = await this.usuarioModel.find();
            return products;
        }

        async getUser(user: string, pass: String): Promise<any> {    
            const product = await this.usuarioModel.find({"user": user, "pass": pass}); 
            return product;
        }
}
